package Binar.Springboottest.controller;

import Binar.Springboottest.model.UserType;
import Binar.Springboottest.model.Users;
import Binar.Springboottest.service.UserTypeService;
import Binar.Springboottest.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class UsersController {

    @Autowired
    private UsersService userService;

    @Autowired
    private UserTypeService userTypeService;

    public String addUser(String username, String password, String email, String typeName){
        userService.saveUsers(username, password, email, typeName);
        return "Add User Success!";
    }

    public void getUser(Integer id){
        Users user = userService.getUser(id).get();
        System.out.println("username : "+ user.getUsername() + "\nemail : " + user.getEmail());
    }

    public void getUserType(String typeName){
        UserType userType = userTypeService.findByTypeName(typeName);
        System.out.println("Type id : " + userType.getTypeId() + "\nType Name : " + userType.getTypeName());
    }
}
