package Binar.Springboottest.controller;

import Binar.Springboottest.service.PrintHaloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class Print {

    @Autowired
    PrintHaloService printHaloService;

    public void  printHalo(){System.out.println(printHaloService.printHalo());
    }
}
