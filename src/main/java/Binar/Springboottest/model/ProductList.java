package Binar.Springboottest.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Setter
@Getter
@Entity
public class ProductList {
    @Id
    private Integer productId;
}
