package Binar.Springboottest.repository;

import Binar.Springboottest.model.UserType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserTypeRepository extends JpaRepository <UserType, Integer>{
    public UserType findByTypeName (String name);

    @Query(nativeQuery = true, value = "select * from user_type")
    public UserType findByeblabla (String name);
}
