package Binar.Springboottest.service;

import Binar.Springboottest.model.UserType;
import Binar.Springboottest.repository.UserTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserTypeServiceImpl implements UserTypeService{

    @Autowired
    private UserTypeRepository userTypeRepository;

    @Override
    public void saveUserType(String typeName){
        UserType userType = new UserType();
        userType.setTypeName(typeName);
        userTypeRepository.save(userType);
    }
    @Override
    public UserType findByTypeName(String typeName){ return userTypeRepository.findByTypeName(typeName);}
}
