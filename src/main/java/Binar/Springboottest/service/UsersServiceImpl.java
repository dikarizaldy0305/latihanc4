package Binar.Springboottest.service;

import Binar.Springboottest.model.UserType;
import Binar.Springboottest.model.Users;
import Binar.Springboottest.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsersServiceImpl implements UsersService{

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private UserTypeService userTypeService;

    @Override
    public void saveUsers(String username, String password, String email, String typeName){
        Users user = new Users();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        UserType userType = userTypeService.findByTypeName(typeName);
        if(userType != null){
            user.setTypeId(userType);
        }else {
            userTypeService.saveUserType(typeName);
            user.setTypeId(userTypeService.findByTypeName(typeName));
        }
        usersRepository.save(user);
    }
    @Override
    public Optional<Users> getUser(Integer userId){ return usersRepository.findById(userId);}
}
