package Binar.Springboottest.service;

import Binar.Springboottest.model.UserType;
import org.springframework.stereotype.Service;

@Service
public interface UserTypeService {
    public void saveUserType(String typeName);
    public UserType findByTypeName(String typeName);
}
