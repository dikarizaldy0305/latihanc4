package Binar.Springboottest.service;

import Binar.Springboottest.model.UserType;
import Binar.Springboottest.model.Users;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface UsersService {

    public void saveUsers(String username, String password, String email, String typeId);
    public Optional<Users> getUser(Integer userId);
}
