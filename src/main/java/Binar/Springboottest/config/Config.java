package Binar.Springboottest.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Value("${passname}")
    private String username;

    @Value("${password}")
    private String password;

    @Bean
    public String coba(){
        System.out.println("Ini Method dijalanin");
        System.out.println("Username "+username);
        System.out.println("Password "+password);
        return "Method dijalanin Nih!";
    }
}
