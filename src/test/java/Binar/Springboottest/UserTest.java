package Binar.Springboottest;

import Binar.Springboottest.controller.UsersController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserTest {

    @Autowired
    private UsersController usersController;
    
    @Test
    @DisplayName("Test ADD User")
    public void addUser(){
        String response = usersController.addUser("Dika","dika123","andika@gmail.com","VIP");
        Assertions.assertEquals("Add User Success!", response);
    }

    @Test
    @DisplayName("Test GET User")
    public void getUser(){usersController.getUser(1);}
    
    @Test
    @DisplayName("Test GET User Type")
    public void getUserType(){usersController.getUserType("VIP");}
}
